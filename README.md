# HAL RockPro64

Some of the files in the RockPro64 HAL component are distributed under the CDDL licence, and I am distributing modified versions. Therefore, to comply with my obligations under the said licence, I am publishing those sources here.

Note that the remainder of the source code for this HAL is distributed under different licence(s) and I am not authorised to publish those at this time. Therefore you should not expect this to build a functional HAL.

Ben Avison, 23 Jan 2022.
